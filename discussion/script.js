// dummy database 
let posts = [];
let count = 1;

// Create/Add Posts
document.querySelector('#form-add-post').addEventListener('submit', (e) => {
	// This prevents the webpage from reloading everytime there's a change in the webpage.
	e.preventDefault();

	// Acts like a schema/model of the document
	posts.push({

		id: count,
		title: document.querySelector('#txt-title').value,
		body: document.querySelector('#txt-body').value
	});
	// This is responsible to increment the value of the ID.
	count++;

	console.log(posts);
	showPost(posts);
	alert("Successfully added");
})

// display/ show the posts
const showPost = (posts) => {

	// Contain all individual posts
	let postEntries = '';

	posts.forEach((post) => {

		postEntries += `
			<div id = 'post-${post.id}'>
				<h3 id='post-title-${post.id}'>${post.title}</h3>
				<p id= 'post-body-${post.id}'>${post.body}</p>
				<button onclick="editPost('${post.id}')">Edit</button>
				<button onclick="deletePost('${post.id}')">Delete</button>
			</div>
		`;
	})

	document.querySelector('#div-post-entries').innerHTML = postEntries;
}

// Edit post
const editPost = (postId) => {

		let title = document.querySelector(`#post-title-${postId}`).innerHTML;
		let body = document.querySelector(`#post-body-${postId}`).innerHTML;

		document.querySelector('#txt-edit-id').value = postId;
		document.querySelector('#txt-edit-title').value = title;
		document.querySelector('#txt-edit-body').value = body;


}

// Updating posts
document.querySelector('#form-edit-post').addEventListener('submit', (e) => {


	e.preventDefault();

	// To find the specific post that we want to edit
	for(let i=0; i < posts.length; i++) {

		// The values post[i].id is a NUMBER while document.querySelector("#txt-edit-id").value is a STRING
		// Therefore, it is necessary to convert the NUMBER to a STRING
		if(posts[i].id.toString() === document.querySelector(`#txt-edit-id`).value) {
			posts[i].title = document.querySelector('#txt-edit-title').value
			posts[i].body = document.querySelector('#txt-edit-body').value

			showPost(posts);
			alert("Successfully Updated!");

			break;
		}
	}
});

// activity 48 - deleting post
const deletePost = (postId) => {

	for(let i = 0; i < posts.length; i++) {

		if(posts[i].id.toString() === document.querySelector("#div-post-entries").value) {

			findIndex(i)
		}
	} 
}